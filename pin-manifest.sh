#!/bin/bash

repomanif="default.xml"
branch="${CI_COMMIT_REF_NAME}"

if [ "${branch}" = "master" ]; then
  git clone -o origin "${CI_REPOSITORY_URL}" .
  repobranch="master"
else
  # branch == auto
  # When checked out by Gitlab, there is no refs/heads/master
  # Workaround: Tag this commit and use that Git reference.
  git tag this-commit
  repobranch="refs/tags/this-commit"
  repomanif="pinned-manifest.xml"
fi

repo init -u . -b ${repobranch} -m ${repomanif}
repo sync --no-clone-bundle -j4
repo manifest -r -o pinned-manifest.xml
